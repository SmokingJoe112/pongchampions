
const http = require('http');
const socket = require('socket.io');
const mv = require('./server/mvPhysics.js');
const mvpg = require('./server/mvPostgres');
const mve = require('./server/mvExpress');

var serv = http.createServer(mve.app);

serv.listen(3000, () => {
    console.log('IO Game started on port 3000');
});

//Socket IO
var netGames = [];
var netPlayers = [];
var gameTs = null;

var io = socket(serv);

const GAME_WIDTH = 800;
const GAME_HEIGHT = 950;

class Ball {
    constructor (xx, yy, fx, fy, r, g){
        this.gameId = g;
        this.radius = r;
        this.speed = 200;
        this._initSpeed = this.speed;
        this.score = {
            top: 0,
            bottom: 0
        };

        this.position = {
            x: xx,
            y: yy
        };

        this.force = {
            x: fx,
            y: fy
        };
    }

    physicsUpdate(dt, p) {
        this.speed += 25 * dt;
        if (this.position.x + this.radius > GAME_WIDTH) {
            this.force.x = -Math.abs(this.force.x);
            this.force.y *= 1.1;
        }
        if (this.position.x - this.radius < 0) {
            this.force.x = Math.abs(this.force.x);
            this.force.y *= 1.1;
        }
        if (this.position.y > GAME_HEIGHT) {
            this.position.x = GAME_WIDTH/2;
            this.position.y = GAME_HEIGHT/2;
            this.force.x = Math.random() - 0.5;
            this.force.y = Math.random() - 0.5;
            this.speed = this._initSpeed;
            this.score.top += 1;
        }
        if (this.position.y < 0) {
            this.position.x = GAME_WIDTH/2;
            this.position.y = GAME_HEIGHT/2;
            this.force.x = Math.random() - 0.5;
            this.force.y = Math.random() - 0.5;
            this.speed = this._initSpeed;
            this.score.bottom += 1;
        }

        if (this.score.top > 2) {
            io.emit('GameEnd', {g: this.gameId, p: 1});
            if (netGames[this.gameId][0][1].username) {
                mvpg.LogWin(netGames[this.gameId][0][1].username);
            }
            if (netGames[this.gameId][0][0].username) {
                mvpg.LogLoss(netGames[this.gameId][0][0].username);
                }
            closeGame(this.gameId);
        }
        if (this.score.bottom > 2) {
            io.emit('GameEnd', {g: this.gameId, p: 0});
            if (netGames[this.gameId][0][0].username) {
                mvpg.LogWin(netGames[this.gameId][0][0].username);
            }
            if (netGames[this.gameId][0][1].username) {
            mvpg.LogLoss(netGames[this.gameId][0][1].username);
            }
            closeGame(this.gameId);
        }

        for (let i = 0; i < p.length; i++) {
            let colliding = mv.cCircleBox({x: this.position.x, y: this.position.y, r: this.radius}, {x: p[i].position.x, y: p[i].position.y, w: p[i].width, h: p[i].height});
            if (colliding === true) {
                let dir = mv.directionTo({x: p[i].position.x + p[i].width/2, y: p[i].position.y + p[i].height/2}, this.position);
                this.force.x += dir.x/2 + mv.normalize(p[i].force).x;
                this.force.y = dir.y * 2;
            }
        }

        var netForce = Math.abs(this.force.x) + Math.abs(this.force.y);
        if (netForce > 0) {
            this.force.x = this.force.x/netForce;
            this.force.y = this.force.y/netForce;
        }

        this.position.x += this.force.x * this.speed * dt;
        this.position.y += this.force.y * this.speed * dt;
    }
}

class Player {
    constructor(xx = 0, yy = 0, username, anch, image) {
        this.speed = 500;
        this.width = 90;
        this.height = 15;
        this.anchor = anch;
        this.username = username;
        this.imageAdress = image;

        this.position = {
            x: xx,
            y: yy
        };

        this.force = {
            x: 0,
            y: 0
        };
    }

    moveCallback(cb) {
        if (cb === 'leftDown') {
            this.force.x = -1;
        }
    
        if (cb === 'rightDown') {
            this.force.x = 1;
        }

        if (cb === 'leftUp' && this.force.x === -1) {
            this.force.x = 0;
        }
    
        if (cb === 'rightUp'&& this.force.x === 1) {
            this.force.x = 0;
        }
    }

    physicsUpdate(dt) {
        if (!dt) return;
        let nextX = this.position.x + this.force.x * dt * this.speed;
        let nextY = this.position.y + this.force.y * dt * this.speed;

        if (nextX < 0) {
            nextX = 0;
        }

        if (nextY < 0) {
            nextY = 0;
        }

        if (nextX + this.width > GAME_WIDTH) {
            nextX = GAME_WIDTH - this.width;
        }
        if (nextY + this.height > GAME_HEIGHT) {
            nextY = GAME_HEIGHT - this.height;
        }

        this.position.x = nextX;
        this.position.y = nextY;
    }
}

function closeGame(g) {
    console.log('Closed game: ' + g);
    for (let i = 0; i < netGames.length; i++) {
        if (i > g) {
            netGames[i][1].gameId --;
        }
    }

    netGames.splice(g, 1);
}

io.on('connection',(sock) => {
    console.log('Received connection');

    sock.on('UserInput', ({gameId, userId, cb}) => {
        if (netGames[gameId]) {  
            netGames[gameId][0][userId].moveCallback(cb);
        }
    })

    sock.on('JoinGameRequest', (username) => {
        sock.emit('NetID', netPlayers.length);

        if (netPlayers.length === 0) {
            let p = new Player(GAME_WIDTH/2-85, GAME_HEIGHT-45, username,'bottom');
            gameTs = Date.now();
            sock.emit('GameID', {g: netGames.length, ts: gameTs});
            netPlayers.push(p);
            netGames.push([netPlayers, null, gameTs, [0, 0], [sock.id, null]]);
        } else if (netPlayers.length === 1) {
            let p = new Player(GAME_WIDTH/2-85, 15, username,'top');
            sock.emit('GameID', {g: netGames.length-1, ts: gameTs});
            netPlayers.push(p);
            netGames[netGames.length-1][0] = netPlayers;
            netGames[netGames.length-1][1] = new Ball(GAME_WIDTH/2,GAME_HEIGHT/2,Math.random()-0.5, Math.random()-0.5 * 2, 15, netGames.length-1);
            netGames[netGames.length-1][3].p2 = true;
            netGames[netGames.length-1][4][1] = sock.id;
            netPlayers = [];
            gameTs = null;
        }
    });

    sock.on('Login', ({username, password}) => {
        if (username && password) {
            console.log("Passed Check ");
            let loginStatus = mvpg.AccountLogin(username, password, (success) => {
                if (success) {
                    sock.emit('PassedLogin', {result: success, uname: username});
                }
            });
        }
    });

    sock.on('LeaderboardRequest', () => {
        leaderboard = mvpg.GetLeaderboard((leaderboard) => {
            sock.emit('LeaderboardPickup', leaderboard.rows);
        });
    });

    sock.on('RejoinGameRequest', ({g, id, ts}) => {

        if (netGames.length > g && netGames[g][2] == ts) {
            netGames[g][4][id] = sock.id;
            sock.emit('RejoinStatus', true); 
        } else {
            sock.emit('RejoinStatus', false);
        }
    });

    sock.on('PingReply', ({g, p}) => {
        if (netGames[g]) {
            netGames[g][3][p] = 0;
        }
    });
});

let passedTime = 0; 
lastTime = Date.now()
const update = () => {
    dt = (Date.now() - lastTime)/1000;
    passedTime += dt;
    if (netGames) {
        for (let g = 0; g < netGames.length; g++) {
            let playersList = netGames[g][0];

            for (let i = 0; i < playersList.length; i++) {
                if (playersList[i]) {
                    playersList[i].physicsUpdate(dt);
                    netGames[g][3][i] += dt;
                }
            }
            try {
                let b = netGames[g][1];
                if (b) {
                    b.physicsUpdate(dt, playersList);
                }

                netGames[g][4].forEach(socket => {
                    if (socket) {
                        io.to(socket).emit('BallPositionUpdate', {g, b});
                    }
                });
            
                if (netGames[g][3][1] > 30 || netGames[g][3][0] > 30) {
                    closeGame();
                    io.emit('GameEnd', {g, p: 0});
                    if (g === netGames.length) {
                        netPlayers = [];
                    }
                    console.log('ClosedGame');
                }

                netGames[g][4].forEach(socket => {
                    if (socket) {
                        io.to(socket).emit('PlayerPositionUpdate', {g, playersList});
                    }
                });

            } catch (e) {
                console.log(`Update loop failed: ${e}`);
            }
        }
    }

    if (passedTime >= 5) {
        io.emit('Ping');
        passedTime = 0;
    }


    lastTime = Date.now();
    setTimeout (update, 0);
};
update();
