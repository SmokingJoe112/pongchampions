const sock = io();

const table = document.querySelector('#leaderboard');

const Refresh = () => {
    sock.emit('LeaderboardRequest');
}

const Printboard = (leaderboards) => {
    for (let i = 0; i < leaderboards.length; i++) {
        let row = table.insertRow(i+1);

        let name = row.insertCell(0);
        name.innerHTML = leaderboards[i].name;

        let wins = row.insertCell(1);
        wins.innerHTML = leaderboards[i].gameswon;

        let losses = row.insertCell(2);
        losses.innerHTML = leaderboards[i].gameslost;
    }
}

sock.on('LeaderboardPickup', (leaderboard) => {
    Printboard(leaderboard);
});

Refresh();