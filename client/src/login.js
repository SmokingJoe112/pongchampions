const sock = io();

const onLoginFormSent = (e) => {
    e.preventDefault();

    const usernameInput = document.querySelector('#username');
    const username = usernameInput.value;

    const passwordInput = document.querySelector('#password');
    const password = passwordInput.value;

    usernameInput.value = '';
    passwordInput.value = '';

    sock.emit('Login', { username, password });
};

sock.on('PassedLogin', ({result, uname}) => {
    if (result) {
        localStorage.setItem('Username', uname);
    }
});

document.querySelector('#login-form').addEventListener('submit', onLoginFormSent);