
var sock = io();

let username = localStorage.getItem('Username')

var score = {
    top: 0,
    bottom: 0
}

var ball = {
    position: {x: 0, y: 0},
    radius: 0,
    force: { x: 0, y: 0}
}

var inGame = false;

let canvas = document.getElementById("game-canvas");
let ctx = canvas.getContext('2d');

const GAME_WIDTH = 800;
const GAME_HEIGHT = 950;

var pList = [[null, null],[new Image(), new Image()]];
pList[1][0].src = 'res/watermelon.png';
pList[1][0].id = "p0img";
pList[1][1].src = 'res/watermelon.png';
pList[1][1].id = "p1img";

class InputHandler {
    constructor (callback) {
        document.addEventListener("keydown", event => {
            switch (event.keyCode) {
                case 37:
                    callback('leftDown');
                    break;
                case 39:
                    callback('rightDown');
                    break;
            }
        });

        document.addEventListener("keyup", event => {
            switch (event.keyCode) {
                case 37:
                    callback('leftUp');
                    break;
                case 39:
                    callback('rightUp');
                    break;
            }
        });
    }
}

const joinGame = () => {
    localStorage.clear();
    console.log('joinRequest');
    sock.emit('JoinGameRequest', username);
}

sock.on('connect', () => {
    if (!inGame) {
        if (!localStorage.getItem('userId') || !localStorage.getItem('gameId') || !localStorage.getItem('gameTimestamp')) {
            joinGame();
        } else {
            console.log('rejoinRequest');
            sock.emit('RejoinGameRequest', {g: JSON.parse(localStorage.getItem('gameId')), id: JSON.parse(localStorage.getItem('userId')),ts: JSON.parse(localStorage.getItem('gameTimestamp'))});
        }
    }
});

sock.on('NetID', (id) => {
    localStorage.setItem('userId', JSON.stringify(id));
    inGame = true;
});
sock.on('GameID', ({g, ts}) => {
    localStorage.setItem('gameId', JSON.stringify(g));
    localStorage.setItem('gameTimestamp', JSON.stringify(ts));
    inGame = true;
});

sock.on('Ping', () => {
    console.log('Ping');
    sock.emit('PingReply', {g: JSON.parse(localStorage.getItem('gameId')), p: JSON.parse(localStorage.getItem('userId'))});
})

sock.on('PlayerPositionUpdate', ({g, playersList}) => {
    if (g == localStorage.getItem('gameId')) {
        ctx.clearRect(0,0,GAME_WIDTH,GAME_HEIGHT);
        for (let i = 0; i < playersList.length; i++) {
            pList[0][i] = playersList[i];
        }
    }
});

sock.on('RejoinStatus', (success) => {
    if (success) {
        inGame = true;
    } else {
        joinGame()
    }
});

sock.on('BallPositionUpdate', ({g, b}) => {
    if (g == localStorage.getItem('gameId')) {
        if (b) {
            ball.position.x = b.position.x;
            ball.position.y = b.position.y;
            ball.force.x = b.force.x;
            ball.force.y = b.force.y;
            ball.radius = b.radius;

            score = b.score;
        }
    }
});

sock.on('GameEnd', ({g, p}) => {
    if (p === localStorage.getItem('userId')) {
        console.log('Win!');
    } else {
        console.log('Loss!');
    }

    if (g == localStorage.getItem('gameId')) {
        localStorage.clear();
        joinGame()
    } else if (g < JSON.parse(localStorage.getItem('gameId'))) {
        localStorage.setItem('gameId', JSON.parse(localStorage.getItem('gameId'))-1);
    }
});

const draw = () => {
    ctx.fillStyle = "#FFF";
    ctx.font = '20px Lucida Console'
    ctx.fillText(score.top, GAME_WIDTH/2, GAME_HEIGHT/2 - 20);
    ctx.fillText(score.bottom, GAME_WIDTH/2, GAME_HEIGHT/2 + 20);

    for (let i = 0; i < pList.length; i++) {
        if (pList[0][i]) {
            if (i == JSON.parse(localStorage.getItem('userId'))) {
                ctx.fillRect(pList[0][i].position.x-1, pList[0][i].position.y-1, pList[0][i].width+2, pList[0][i].height+2);
            }
            ctx.drawImage(pList[1][i], pList[0][i].position.x, pList[0][i].position.y, pList[0][i].width, pList[0][i].height);
        }
    }
    
    ctx.beginPath();
    ctx.strokeStyle = '#FFF';
    ctx.lineWidth = 2;
    ctx.arc(ball.position.x, ball.position.y, ball.radius, 0, 2 * Math.PI, false);
    ctx.stroke();
    
    requestAnimationFrame(draw);
}
requestAnimationFrame(draw);

const inputCallback = (cb) => {
    if(inGame) {
        let userId = localStorage.getItem('userId');
        let gameId = localStorage.getItem('gameId');
        sock.emit('UserInput', {gameId, userId, cb});
    }
}

const ih = new InputHandler(inputCallback);