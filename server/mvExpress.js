
const path = require('path');
const express = require('express');
const router = express.Router();

var app = express();

app.get('/', (req, res, next) => {
    res.sendFile(path.resolve(__dirname+'/../client/index.html'));
});
app.use(express.static(path.resolve(__dirname+'/../client')));

module.exports = {
    app
}