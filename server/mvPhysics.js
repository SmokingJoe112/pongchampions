const clamp = (num, min, max) => {
    if (num < min) {
        return min;
    } else if (num > max ) {
        return max;
    } else {
        return num;
    }
}

// vector = {x, y}
const normalize = (vector) => {
    let t = Math.abs(vector.x) + Math.abs(vector.y);
    if (t > 0) {
        let f = {
            x: vector.x/t,
            y: vector.y/t
        }
        return f;
    } else {
        return vector;
    }
    
}
// {x, y}
const directionTo = (o, d) => {
    r = {
        x: d.x - o.x,
        y: d.y - o.y
    }
    return normalize(r);
}

// pos = {x, y}
const distBetween = (pos1,pos2) => {
    let xDist = pos1.x - pos2.x;
    let yDist = pos1.y - pos2.y;

    let distance = xDist^2 + yDist^2;
    return distance;
}

// Circle = {x, y, r}, box = {x, y, w, h}
const cCircleBox = (circle, box) => {
    if (circle.x + circle.r >= box.x && circle.x - circle.r <= box.x+box.w) {
        if (circle.y + circle.r >= box.y && circle.y - circle.r <= box.y+box.h) {
            let pointX = {
                x: clamp(circle.x, box.x, box.x + box.w),
                y: null
            }
            let pointY = {
                x: null,
                y: clamp(circle.x, box.y, box.y + box.h)
            }

            if (circle.x === pointX.x && circle.y === pointY.y) {
                callback(true);
            }

            if (circle.x < box.x + box.w/2) {
                pointY.x = box.x;
            } else {
                pointY.x = box.x + box.w;
            }

            if (circle.y > box.y + box.h/2) {
                pointX.y = box.y;
            } else {
                pointX.y = box.y;
            }

            if ((distBetween({x: circle.x, y: circle.y}, pointX) <= circle.r^2) || (distBetween({x: circle.x, y: circle.y}, pointY) <= circle.r^2)) {
                return true;
            }
            return false;
        }
    } else {
        return false;
    }
}

module.exports = {
    clamp,
    normalize,
    directionTo,
    distBetween,
    cCircleBox
}