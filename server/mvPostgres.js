
const pgClient = require('pg').Client;
const bcrypt = require('bcrypt');

var clientArgs = {
    user: process.argv[0],
    password: process.argv[1],
    host: process.argv[2],
    port: 5432,
    database: process.argv[3]
}

/*
async function databaseFunction() {
    try {
        await dbClient.connect();
        console.log('Connected successfully.');
    } catch (e) {
        console.log(e);
    } finally {
        await dbClient.end();
    }
}
*/

async function AccountLogin(uname, password, callback) {
    var dbClient = new pgClient(clientArgs);
    var cbresult = null;
    var account = null;
    try {
        await dbClient.connect();
        account = await dbClient.query('SELECT * FROM accounts WHERE username = $1', [uname]);
        if (account.rows[0]) {
            let matching = await bcrypt.compare(password, account.rows[0].password);
            if (matching) {
                cbresult = true;
            } else {
                cbresult = false;
            }
        } else {
            let hashPass = await bcrypt.hash(password, 15);
            await dbClient.query('INSERT INTO accounts (username, password) VALUES ($1, $2)', [uname, hashPass]);
            await dbClient.query('INSERT INTO leaderboard (name, gameswon, gameslost, winloss, rating) VALUES ($1, 0, 0, 0, 0)', [uname]);
            console.log('New User has been made!');
            cbresult = true;
        }
    } catch (e) {
        console.log(e);
    } finally {
        await dbClient.end();
        callback(cbresult);
    }
}

async function LogWin(uname) {
    var dbClient = new pgClient(clientArgs);
    try {
        await dbClient.connect();
        await dbClient.query('UPDATE leaderboard SET gameswon = gameswon + 1 WHERE name = $1', [uname]);
    } catch (e) {
        console.log(e);
    } finally {
        await dbClient.end();
    }
}

async function LogLoss(uname) {
    var dbClient = new pgClient(clientArgs);
    try {
        await dbClient.connect();
        await dbClient.query('UPDATE leaderboard SET gameslost = gameslost + 1 WHERE name = $1', [uname]);
    } catch (e) {
        console.log(e);
    } finally {
        await dbClient.end();
    }
}

async function GetLeaderboard(callback) {
    var dbClient = new pgClient(clientArgs);
    leaderboard = null;
    try {
        await dbClient.connect();
        leaderboard = await dbClient.query('SELECT * FROM leaderboard');
        callback(leaderboard);
    } catch (e) {
        console.log(e);
    } finally {
        await dbClient.end();
    }
}

module.exports = {
    AccountLogin,
    LogWin,
    LogLoss,
    GetLeaderboard
}